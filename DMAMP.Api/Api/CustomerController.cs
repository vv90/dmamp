﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DMAMP.Models;
using DMAMP.Repositories.Interfaces;

namespace DMAMP.Api
{
    public class CustomerController : ApiController
    {
	    private readonly IUnitOfWork _unitOfWork;
	    private readonly IRepository<Customer> _customerRepository;

	    public CustomerController(IUnitOfWork unitOfWork)
	    {
		    _unitOfWork = unitOfWork;
		    _customerRepository = unitOfWork.GetRepository<Customer>();
	    }

	    public IEnumerable<Customer> Get()
	    {
		    return _customerRepository.Get();
	    }
    }
}

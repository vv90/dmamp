﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMAMP.Models.Interfaces
{
	public interface IEntity
	{
		int Id { get; set; }
		DateTime CreatedDate { get; set; }
		DateTime ModifiedDate { get; set; }
	}
}

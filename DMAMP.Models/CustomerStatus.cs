﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMAMP.Models
{
	public enum CustomerStatus
	{
		Unknown = 0,
		InitialCall,
		OpenLesson,
		Novice,
		Advanced
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMAMP.Models;

namespace DMAMP.Data.Mappings
{
	public class CustomerMap : EntityTypeConfiguration<Customer>
	{
		public CustomerMap()
		{
			ToTable("Customer");

			HasKey(x => x.Id);
			Property(x => x.Id)
				.HasColumnName("Id")
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
				.IsRequired();

			Property(x => x.CreatedDate)
				.IsRequired();
			Property(x => x.ModifiedDate)
				.IsRequired();
			Property(x => x.FirstName)
				.IsRequired();
			Property(x => x.LastName);
			Property(x => x.Phone);
			Property(x => x.Email);
			Property(x => x.Status)
				.IsRequired();
		}
	}
}

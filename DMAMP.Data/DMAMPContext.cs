﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMAMP.Data.Mappings;
using DMAMP.Models;

namespace DMAMP.Data
{
    public class DMAMPContext : DbContext
    {
		DbSet<Customer> Customers { get; set; }

	    public DMAMPContext() : base("Name=DMAMPDatabase")
	    {
			base.Configuration.ProxyCreationEnabled = false;
	    }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new CustomerMap());
			base.OnModelCreating(modelBuilder);
		}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMAMP.Data;
using DMAMP.Repositories.Exceptions;
using DMAMP.Repositories.Interfaces;

namespace DMAMP.Repositories
{
	public class UnitOfWork : IDisposable, IUnitOfWork
	{
		private Dictionary<Type, object> repositoryDictionary;
		private DMAMPContext context = new DMAMPContext();
		private bool disposed = false;

		public UnitOfWork()
		{
			repositoryDictionary = new Dictionary<Type, object>();
		}

		public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, Models.Interfaces.IEntity
		{
			object repository = null;

			if (repositoryDictionary.TryGetValue(typeof (TEntity), out repository) == false)
			{
				repository = new Repository<TEntity>(context);
				repositoryDictionary.Add(typeof(TEntity), repository);
			}

			if (!(repository is IRepository<TEntity>))
				throw new RepositoryTypeException();

			return repository as IRepository<TEntity>;
		}

		public void Save()
		{
			context.SaveChanges();
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					context.Dispose();
				}
			}
			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMAMP.Repositories.Exceptions
{
	/// <summary>
	/// The exception that is thrown when a method attempts to use a repository with a wrong generic type
	/// </summary>
	public class RepositoryTypeException : Exception
	{
		public RepositoryTypeException()
		{
		}

		public RepositoryTypeException(string message)
			: base(message)
		{
		}

		public RepositoryTypeException(string message, Exception innerException) 
			: base(message, innerException)
		{
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMAMP.Models.Interfaces;

namespace DMAMP.Repositories.Interfaces
{
	public interface IUnitOfWork
	{
		IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity;
		void Save();
	}
}

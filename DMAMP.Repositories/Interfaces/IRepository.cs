﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DMAMP.Models.Interfaces;

namespace DMAMP.Repositories.Interfaces
{
	public interface IRepository<TEntity> where TEntity : class, IEntity
	{
		IEnumerable<TEntity> Get(
			Expression<Func<TEntity, bool>> filter = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			string includeProperties = "");

		TEntity GetById(object id);
		void Insert(TEntity entity);
		void Delete(TEntity entity);
		void Delete(object id);
		void Update(TEntity entity);
	}
}

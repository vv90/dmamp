﻿CREATE TABLE [dbo].[Customer]
(
	[Id] INT IDENTITY (1,1) NOT NULL PRIMARY KEY, 
    [FirstName] NVARCHAR(50) NOT NULL, 
    [LastName] NVARCHAR(50) NULL, 
    [Phone] NVARCHAR(15) NULL, 
	[Email] NVARCHAR(50) NULL,
    [Status] INT NOT NULL DEFAULT 0, 
    [CreatedDate] DATETIME NOT NULL DEFAULT getdate(), 
    [ModifiedDate] DATETIME NOT NULL DEFAULT getdate()
)
